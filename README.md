# MLF2 File Downloader

This application will download files created by a specific MLF2 float over a specified time range.

## Installation

Download a binary release for Linux, MacOS (darwin), or Windows from the [Downloads section](https://bitbucket.org/uwaplmlf2/getmlf2files/downloads/) of this repository and extract onto your system. All binaries are distributed in gzip-compressed TAR archives.

## Usage

``` shellsession
$ getmlf2files --help
Usage: getmlf2files [options] floatid starttime [endtime]

Download all data files from floatid between startime and endtime. Times
must be specified as YYYY-mm-dd[THH:MM[:SS]]. The endtime is optional and
defaults to now.
  -address string
    	MLF2 database server (default "mlf2data.apl.uw.edu:443")
  -debug
    	Enable debugging output
  -exclude string
    	Regexp of files to exclude
  -include string
    	Regexp of files to include
  -outdir string
    	Output directory (default ".")
  -version
    	Show program version information and exit
```

The `-include` and `-exclude` options take wildcard patterns and can be used to filter the list of files to be downloaded.

### Examples

#### Download only ZIP archives

``` shellsession
$ getmlf2files -include='*.zip' 92 2018-11-30 2018-12-01
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a28f056
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a3547c5
2019/05/09 15:47:45 Downloading data0429.zip
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a289e57
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a2fe3a0
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a3017f7
2019/05/09 15:47:45 Downloading data0430.zip
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a2f9f72
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a3b5d58
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a351174
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a3b8480
2019/05/09 15:47:45 Downloading data0431.zip
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a4062e9
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a3b3485
2019/05/09 15:47:45 Checking document: dfef9793fb8d3fd48c3e02139a408669
2019/05/09 15:47:45 Downloading data0432.zip
```

#### Download everything except PNG images

All files are written to the subdirectory `f92`, the subdirectory will be created if necessary.

``` shellsession
$ getmlf2files -outdir=./f92 -exclude='*.png' 92 2018-11-30 2018-12-01
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a28f056
2019/05/09 15:49:11 Downloading env0429.nc
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a3547c5
2019/05/09 15:49:11 Downloading data0429.zip
2019/05/09 15:49:11 Downloading data0429.sx
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a289e57
2019/05/09 15:49:11 Downloading ql-20181130_035518.nc
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a2fe3a0
2019/05/09 15:49:11 Downloading env0430.nc
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a3017f7
2019/05/09 15:49:11 Downloading data0430.zip
2019/05/09 15:49:11 Downloading data0430.sx
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a2f9f72
2019/05/09 15:49:11 Downloading ql-20181130_095603.nc
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a3b5d58
2019/05/09 15:49:11 Downloading env0431.nc
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a351174
2019/05/09 15:49:11 Downloading ql-20181130_153102.nc
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a3b8480
2019/05/09 15:49:11 Downloading data0431.zip
2019/05/09 15:49:11 Downloading data0431.sx
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a4062e9
2019/05/09 15:49:11 Downloading env0432.nc
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a3b3485
2019/05/09 15:49:11 Downloading ql-20181130_212856.nc
2019/05/09 15:49:11 Checking document: dfef9793fb8d3fd48c3e02139a408669
2019/05/09 15:49:11 Downloading data0432.zip
2019/05/09 15:49:11 Downloading data0432.sx
```
