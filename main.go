// Getmlf2files downloads one or more data files from the MLF2 CouchDB
// database.
package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/uwaplmlf2/couchdb"
	"github.com/gobwas/glob"
	"github.com/pkg/errors"
)

var usage = `Usage: getmlf2files [options] floatid starttime [endtime]

Download all data files from floatid between startime and endtime. Times
must be specified as YYYY-mm-dd[THH:MM[:SS]]. The endtime is optional and
defaults to now.
`

var Version = "dev"
var BuildDate = "unknown"

const (
	TFORMAT  = "2006-01-02T15:04:05-0700"
	DBSERVER = "mlf2data.apl.uw.edu:443"
	DBNAME   = "mlf2db"
	VIEW     = "status/alldata"
)

// File attachment description
type fileDesc struct {
	ContentType string `json:"content_type"`
	Type        string `json:"type"`
	Revpos      int    `json:"revpos"`
	Length      int    `json:"length"`
	Stub        bool   `json:"stub"`
}

type fileDoc struct {
	Id          string              `json:"_id,omitempty"`
	Rev         string              `json:"_rev,omitempty"`
	Tstart      int                 `json:"t_start"`
	Tstop       int                 `json:"t_stop"`
	Filetype    string              `json:"filetype"`
	Attachments map[string]fileDesc `json:"_attachments,omitempty"`
}

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug       = flag.Bool("debug", false, "Enable debugging output")
	dbServer    = flag.String("address", DBSERVER, "MLF2 database server")
	outDir      = flag.String("outdir", ".", "Output directory")
	fileInclude = flag.String("include", "", "Regexp of files to include")
	fileExclude = flag.String("exclude", "", "Regexp of files to exclude")
)

// Parse_datetime is a flexible ISO8601-ish date/time parser. It handles
// the following formats (missing least-significant values are assumed
// to be zero):
//
//    YYYY-mm-ddTHH:MM:SS-ZZZZ
//    YYYY-mm-ddTHH:MM:SS   (UTC assumed)
//    YYYY-mm-ddTHH:MM
//    YYYY-mm-dd
//
func parse_datetime(dt string) (time.Time, error) {
	t, err := time.Parse("2006-01-02T15:04:05-0700", dt)
	if err != nil {
		t, err = time.Parse("2006-01-02T15:04:05", dt)
		if err != nil {
			t, err = time.Parse("2006-01-02T15:04", dt)
			if err != nil {
				t, err = time.Parse("2006-01-02", dt)
			}
		}
	}

	return t, err
}

func downloadFiles(db *couchdb.Database, datadir string, files <-chan fileDoc,
	keep, discard glob.Glob) error {
	// Create all directories if needed. We can safely ignore the
	// returned error because it will be caught when we try to
	// open the file.
	os.MkdirAll(datadir, 0755)

	for fd := range files {
		log.Printf("Checking document: %s", fd.Id)
		for k, _ := range fd.Attachments {
			if discard != nil && discard.Match(strings.ToLower(k)) {
				continue
			}
			if keep != nil && !keep.Match(strings.ToLower(k)) {
				continue
			}
			log.Printf("Downloading %s", k)

			path := filepath.Join(datadir, k)
			f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0644)
			if err != nil {
				return errors.Wrapf(err, "file open %q", path)
			}
			_, err = db.GetAttachment(fd.Id, k, f)
			if err != nil {
				log.Print(err)
			}
			f.Close()
		}
	}

	return nil
}

func dbLookup(db *couchdb.Database, view string,
	floatid int, tstart, tend time.Time) (<-chan fileDoc, error) {
	opts := make(map[string]interface{})
	opts["descending"] = false
	opts["reduce"] = false
	opts["startkey"] = []interface{}{floatid, tstart.Format(TFORMAT)}
	opts["endkey"] = []interface{}{floatid, tend.Format(TFORMAT)}

	rows, err := db.Query(couchdb.NewView(view), opts)
	if err != nil {
		return nil, err
	}

	ch := make(chan fileDoc, 1)
	go func() {
		defer close(ch)
		for rows.Next() {
			doc := fileDoc{}
			err := rows.ScanValue(&doc)
			if err != nil {
				log.Printf("Document decode error: %v", err)
				return
			}
			ch <- doc
		}
	}()

	return ch, nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		t_start, t_end time.Time
		err            error
	)

	floatid, _ := strconv.Atoi(args[0])
	t_start, err = parse_datetime(args[1])
	if err != nil {
		log.Fatalf("Bad start time: %q (%v)", args[1], err)
	}

	if len(args) > 2 {
		t_end, err = parse_datetime(args[2])
		if err != nil {
			log.Fatalf("Bad end time: %q (%v)", args[2], err)
		}
	} else {
		t_end = time.Now().UTC()
	}

	var keepRe, discardRe glob.Glob
	if *fileInclude != "" {
		keepRe, err = glob.Compile(strings.ToLower(*fileInclude))
		if err != nil {
			log.Fatalf("Invalid regexp: %q (%v)", *fileInclude, err)
		}
	}

	if *fileExclude != "" {
		discardRe, err = glob.Compile(strings.ToLower(*fileExclude))
		if err != nil {
			log.Fatalf("Invalid regexp: %q (%v)", *fileExclude, err)
		}
	}

	u := url.URL{}
	u.Host = *dbServer
	if strings.HasSuffix(*dbServer, ":443") {
		u.Scheme = "https"
	} else {
		u.Scheme = "http"
	}
	db := couchdb.NewDatabase(u.String(), DBNAME)
	if db == nil {
		log.Fatal("Database URL error")
	}

	ch, err := dbLookup(db, VIEW, floatid, t_start, t_end)
	if err != nil {
		log.Fatalf("Database lookup failed: %v", err)
	}

	err = downloadFiles(db, *outDir, ch, keepRe, discardRe)
	if err != nil {
		log.Fatal(err)
	}
}
