module bitbucket.org/uwaplmlf2/getmlf2files

require (
	bitbucket.org/uwaplmlf2/couchdb v0.6.0
	github.com/gobwas/glob v0.2.3
	github.com/pkg/errors v0.8.1
)

go 1.13
